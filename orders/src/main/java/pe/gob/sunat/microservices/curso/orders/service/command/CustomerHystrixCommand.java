package pe.gob.sunat.microservices.curso.orders.service.command;

import java.util.function.Function;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

import pe.gob.sunat.microservices.curso.customers.client.CustomerServiceClient;

/**
 * @author jyauyo
 * 
 * Clase Command Generica para CustomerHystrix
 * 
 * */
public class CustomerHystrixCommand<T> extends HystrixCommand<T> {

	public static final int CONCURRENT_REQUESTS = 20;
	private final CustomerServiceClient customerServiceClient;
	private final Function<CustomerServiceClient, T> callback;
	private T retorno;
	
	public CustomerHystrixCommand(CustomerServiceClient customerServiceClient, 
			Function<CustomerServiceClient, T> callback) {
		super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("GroupComandoLatencia"))
				.andCommandPropertiesDefaults(HystrixCommandProperties.Setter()
						.withExecutionIsolationSemaphoreMaxConcurrentRequests(CONCURRENT_REQUESTS)));
		this.customerServiceClient = customerServiceClient;
		this.callback = callback;
	}

	@Override
	protected T run() throws Exception {
		retorno = callback.apply(customerServiceClient);
		return retorno;
	}

	@Override
	protected T getFallback() {
		System.err.println("Fallback - msCustomer-cliente");		
		return retorno;
	}
}
