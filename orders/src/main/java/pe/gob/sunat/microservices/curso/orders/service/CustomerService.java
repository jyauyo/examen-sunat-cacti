package pe.gob.sunat.microservices.curso.orders.service;

import java.io.IOException;
import java.util.List;

import pe.gob.sunat.microservices.curso.customers.client.Address;
import pe.gob.sunat.microservices.curso.customers.client.Customer;
import pe.gob.sunat.microservices.curso.customers.client.CustomerServiceClient;
import pe.gob.sunat.microservices.curso.orders.service.command.CustomerHystrixCommand;
import retrofit2.Response;

/**
 * @author jyauyo
 * 
 * <br><br>Clase Service del ms-customer
 * 
 * */
public class CustomerService {
	private final CustomerServiceClient customerServiceClient;

	public CustomerService(CustomerServiceClient customerServiceClient) {
		this.customerServiceClient = customerServiceClient;
	}

	/**
	 * @author jyauyo
	 * 
	 * Valida si existe el cliente
	 * 
	 * @return Boolean
	 * 
	 * */
	public Boolean existeCustomer(Long idCustomer, String credentials) {

		Response<Customer> rsp = new CustomerHystrixCommand<>(customerServiceClient, s -> {
			try {
				return s.getCustomer(idCustomer, credentials).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}).execute();
		
		Customer cu = validarResponse(rsp);
		return (cu != null) ? Boolean.TRUE : Boolean.FALSE;
	}
	
	/**
	 * @author jyauyo
	 * 
	 * Obtiene las direcciones del Customer
	 * 
	 * @return List<Address>
	 * 
	 * */
	public List<Address> addressByCustomer(Long idCustomer, String credentials){
		
		Response<List<Address>> rsp = new CustomerHystrixCommand<>(customerServiceClient, s -> {
			try {
				return s.getAddressByCustomer(idCustomer, credentials).execute();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}).execute();
		
		return validarResponse(rsp);		
	}

	/**
	 * @author jyauyo
	 * 
	 * @param rsp
	 * Response: Response del microservicio invocado
	 * 
	 * <br>
	 * <br>
	 * 	Valida el Response del ms-Customer
	 * 
	 */
	private <T> T validarResponse(Response<T> rsp) {

		if (rsp != null) {
			if (rsp.code() >= 200 && rsp.code() < 400) {
				return rsp.body();
			}
		}
		return null;
	}
}
