package pe.gob.sunat.microservices.curso.customers.service;

import pe.gob.sunat.microservices.curso.customers.service.command.OrderServiceRemoteInvokerCommand;
import pe.gob.sunat.microservices.curso.orders.client.OrderServiceClient;

/**
 * @author jyauyo
 * 
 * */
public class OrderService {
	
	private final OrderServiceClient orderServiceClient;
	
	public OrderService(OrderServiceClient orderServiceClient){
		this.orderServiceClient = orderServiceClient;
	}
	
	public Boolean existeOrderByCustomer(Long idCustomer, String credentials) {
            System.out.println("OrderService.existeOrderByCustomer");
	    return
	      new OrderServiceRemoteInvokerCommand(orderServiceClient, idCustomer, credentials)
        .execute();
	}

}
