package pe.gob.sunat.microservices.curso.customers.client;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface CustomerServiceClient {

  @GET("v1/customers/{id}")
  Call<Customer> getCustomer(@Path("id") Long idCustomer, @Header("Authorization") String credentials);
  
  @GET("v1/customers/{id}/addresses")
  Call<List<Address>> getAddressByCustomer(@Path("id") Long idCustomer, @Header("Authorization") String credentials);
}


