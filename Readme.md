# exportar la variable del host de Postgres
export DATABASE_HOST=localhost


# Levantamos el Docker Compose
./up.sh

# Validanmos con
docker-compose ps

#Asegurarnos que corren todos los contenedores (Up)

# Generamos los tags de la migración
java -jar ./build/libs/service.jar db tag ./src/main/resources/config.yaml 2018_08_15_19_42

# Aplicamos la migración a la base de datos
java -jar ./build/libs/service.jar db migrate ./src/main/resources/config.yaml


# ============== Comandos utilitarios =================

# Usar el cliente de Postgres, el password es c7
psql -h localhost -U c7 -W c7_clientes

# Listar volumenes de datos que Docker gestiona
docker volume ls

# Limpiar los volumenes (si deseo borrarlos, no se recuperan)
docker volume rm id_del_volumen

